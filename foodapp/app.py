from flask import Flask, render_template, g, request
import sqlite3
from datetime import  datetime

app = Flask(__name__)


def connect_db():
    sql = sqlite3.connect('/home/waters/PycharmProjects/py4e/foodapp/food_log.db')
    sql.row_factory = sqlite3.Row
    return sql


def get_db():
    if not hasattr(g, 'sqlite3_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    if hasattr(g,'sqlite_db'):
        g.sqlite_db.close()


@app.route('/',methods = ['POST', 'GET'])
def index():

    db = get_db()

    if request.method == 'POST':
        date = request.form['date']
        dt = datetime.strptime(date,'%Y-%m-%d')
        databse_date = datetime.strftime(dt, '%Y%m%d')
        db.execute(" insert into log_date('entry_date') values (?) ",[databse_date])
        db.commit()
    cur = db.execute('select entry_date from log_date')
    results = cur.fetchall()

    pretty_date = []

    for i in results:
        single_date = {}
        d = datetime.strptime(str(i['entry_date']),'%Y%m%d')
        single_date['entry_date'] = datetime.strftime(d, '%B %d, %Y')
        pretty_date.append(single_date)
    return render_template('home.html', results=pretty_date)


@app.route('/view/<date>')
def view(date):
    db = get_db()

    cur = db.execute('select entry_date from log_date where entry_date = ?', [date])
    result = cur.fetchone()
    d = datetime.strptime(str(result['entry_date']),'%Y%m%d')
    pretty_date = datetime.strftime(d,'%B %d, %Y')
    return render_template('day.html',date=pretty_date)


@app.route('/food',methods= ['POST', 'GET'])
def food():
    db = get_db()
    if request.method == 'POST':
        food = request.form['food-name']
        protein = int(request.form['protein'])
        carb = int(request.form['carb'])
        fat = int(request.form['fat'])

        calories = protein * 4 + carb * 4 + fat * 9


        db.execute("insert into food ( 'name','protein','carbohydrates','fat' ,'calories') values (?,?,?,?,?) ",[food,protein,carb,fat,calories])
        db.commit()
    cur = db.execute('select name,protein,carbohydrates,fat,calories from food')
    results = cur.fetchall()

    return render_template('add_food.html', results=results)


if __name__ == '__main__':
    app.run(debug=True)
