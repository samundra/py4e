"""
double getting the mail most received
"""
fhand = open('mbox-short.txt')
logcount = dict()
for line in fhand:
    words = line.split()
    if (len(words) < 2) or (words[0] != 'From'): continue
    logcount[words[1]] = logcount.get(words[1], 0) + 1

largest = None
for key in logcount:
    if (largest is None) or (logcount[key] > logcount[largest]):
        largest = key

print(largest, logcount[largest])

