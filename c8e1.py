def chop(choplist):
    """
    The function chops first and last element
    :param choplist:
    :return None:
    """
    del choplist[0], choplist[-1]


def middle(mlist):
    """
    returns the new list without first and last element

    :param mlist:
    :return newlist:
    """
    return mlist[1:-1]


a = [1, 2,  2, 3, 4, 5, 6]

chop(a)
print(a)

b = middle(a)
print(a, b)

