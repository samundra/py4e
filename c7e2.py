fname = input("Enter a file name :")

try:
    mbox = open(fname)
except:
    print("File does not Exist")
    exit()

count = 0
conftotal = 0
confavg = 0
for line in mbox:
    if line.startswith("X-DSPAM-Confidence:"):
        ind = line.find(":")
        fnum = float(line[ind+1:])
        count +=1
        conftotal += fnum

if count > 0:
    confavg = conftotal/count

print("Average spam confidence:", confavg)

