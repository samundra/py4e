"""
program to find frequency of letter in a file in percentage
"""
import string

fname = input("Enter a file name :")

try:
    lfreq = open(fname)
except:
    print("File does not Exist")
    exit()

dicolist = dict()
count = 0
for line in lfreq:
    # removing the extra enter and then punctuation and digit and space
    line = line.rstrip()
    line = line.translate(line.maketrans('', '', string.punctuation))
    line = line.translate(line.maketrans('', '', ' 1234567890'))

    for letter in line.lower():
        dicolist[letter] = dicolist.get(letter,0) + 1
        count += 1

listletter = sorted([ (k,v) for k,v in dicolist.items()])

for k,v in listletter:
    print(k,str(round((v/count)* 100,2))+ "%")

