"""
double getting the mail log
"""
fhand = open('mbox-short.txt')
logcount = dict()
for line in fhand:
    words = line.split()
    if (len(words) < 2) or (words[0] != 'From'): continue
    logcount[words[1]] = logcount.get(words[1], 0) + 1

print(logcount)