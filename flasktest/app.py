from flask import Flask,jsonify, request, url_for , redirect, session, render_template, g
import sqlite3

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'Thisissecretkey!'


def connect_db():
    sql = sqlite3.connect('/home/waters/Documents/data.db')
    sql.row_factory = sqlite3.Row
    return sql


def get_db():
    if not hasattr(g,'sqlite3'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    if hasattr(g,'sqlite3'):
        g.sqlite_db.close()


@app.route('/')
def index():
    return "<h1>Hello world This is a nice day</h1>"


@app.route('/home', methods = ['POST','GET'], defaults = { "name":"Defaults"})
@app.route('/home/<string:name>',methods = ['POST','GET'])
def home(name):

    db = get_db()
    cur = db.execute('select id,name,location from users')
    results = cur.fetchall()
    session['name'] = name
    return render_template('home.html',name = name,d=1,c={'a':1,'b':2},results = results)


@app.route('/json')
def json():
    name = session['name']
    return jsonify({"key":"value","key2" : [1,2,3,4,5,6],"name":name})


@app.route('/query')
def query():
    name = request.args.get("name")
    location = request.args.get('location')
    return "This is a query string {} {}".format(name,location)


@app.route('/theform')
def theform():
    return render_template('theform.html')


@app.route('/theform', methods = ['POST'])
def process():
    name = request.form['name']
    location = request.form['location']

    db = get_db()
    db.execute('insert into users ("name","location") values (?,?)',[name,location])
    db.commit()
    return redirect(url_for('home',name = name))


@app.route('/processjson', methods =['POST'])
def processjson():
    data = request.get_json()
    name =data["name"]
    location = data["location"]

    return jsonify({"name":name,"location":location})


@app.route('/viewresults')
def viewresults():
    db = get_db()
    cur = db.execute('select id,name,location from users')
    results = cur.fetchall()
    return  '<h1>This is the id {}.and the name is {} and the address is {}</h1>'.format(results[1]['id'], results[1]['name'],results[1]['location'])


if __name__ == '__main__':
    app.run()