fcase = open("approvals.txt")

total_approvals = 0

for line in fcase:

    sline = line.split("|")

    if sline[1] == "Central":
        total_approvals += int(sline[2])

print("Total number of approvals in the Central district for year 2016 is", total_approvals)
