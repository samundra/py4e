
fname = input("Enter a file name :")

try:
    mbox = open(fname)
except:
    print("File does not Exist")
    exit()

for line in mbox:
    # for removing the extra next line
    line = line.rstrip()
    print(line.upper())
