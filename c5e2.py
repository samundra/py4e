nmax = None
nmin = None

while True:

    num = input("Enter a number : ")

    if num == "done":
        break

    #testing wether the user have put the valid input that is number
    try:
        num = float(num)

    except:
        print("Invalid input")
        continue


    #for finding the maximum
    if nmax is None:
        nmax = num

    elif nmax < num:
        nmax = num


    #for finding the minimum
    if nmin is None:
        nmin = num

    elif nmin > num:
        nmin = num


print("maximum :", nmax ,"minimum :",nmin)
