"""
double getting the domain mail log
"""
fhand = open('mbox-short.txt')
logcount = dict()
for line in fhand:
    words = line.split()

    # protection for list length and if the second word is not email
    if (len(words) < 2) or (words[0] != 'From') or ('@' not in words[1]): continue
    domain = words[1].split('@')
    logcount[domain[1]] = logcount.get(domain[1], 0) + 1

print(logcount)