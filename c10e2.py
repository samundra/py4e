"""
 hour count with tuple
"""
fhand = open('mbox-short.txt')
logcount = dict()
lmail =[]
for line in fhand:
    words = line.split()
    if (len(words) < 6) or (words[0] != 'From'): continue
    h,m,s = words[5].split(":")
    logcount[h] = logcount.get(h, 0) + 1
lmail = sorted([(k,v) for k, v in logcount.items()])
for k,v in lmail:
    print(k,v)