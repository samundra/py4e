"""
double getting the week day
"""
fhand = open('mbox-short.txt')
dcount = dict()
for line in fhand:
    words = line.split()
    if (len(words) < 3) or (words[0] != 'From'): continue
    dcount[words[2]] = dcount.get(words[2], 0) + 1

print(dcount)
