def computegrade(s):
    try:
        s = float(s)
        grade = "Bad Score"

        if s < 1:
            if s >= 0.9:
                grade = "A"
            elif s >= 0.8:
                grade = "B"
            elif s >= 0.7:
                grade = "C"
            elif s >= 0.6:
                grade = "D"
            elif s < 0.6:
                grade = "F"
            return grade

        else:
            print("Bad score")
    except:
        print("Bad score")


computegrade(0.5)
computegrade(0.95)
computegrade(8)
