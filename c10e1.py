"""
 the mail log with tuple
"""
fhand = open('mbox-short.txt')
logcount = dict()
lmail =[]
for line in fhand:
    words = line.split()
    if (len(words) < 2) or (words[0] != 'From'): continue
    logcount[words[1]] = logcount.get(words[1], 0) + 1

lmail = sorted([(v,k) for k,v in logcount.items()],reverse=True)
v,k = lmail[0]
print(k,v)